#include "cursesUtils.h"

chstring operator |(chstring a, int attr) {
	chstring temp;
	for(auto x : a)
		temp.push_back(x | attr);
	return temp;
} 

chstring chstr(const std::string& str, chtype attr){
	chstring temp;
	for(auto x : str)
		temp.push_back(chtype(x) | attr);
	return temp;
}

window::window(size_t h, size_t w, size_t y, size_t x, int CB, int FC) : 
		decor(newwin(h, w, y, x)), 
		win(derwin(decor, h-2, w-2, 1, 1)), 
		cb(CB), 
		fc(FC) {
	wattron(decor, CB);
	wattron(win, FC);
	redraw();
}
void window::update(size_t h, size_t w, size_t y, size_t x) {
	mvwin(decor, y, x);
	wresize(decor, h, w);
	wresize(win, h-2, w-2);
	redraw();
}
void window::redraw() {
	wclear(decor);
	box(decor, 0, 0);
	wclear(win);
	wrefresh(decor);
	wrefresh(win);
}
window::~window() {
	delwin(win);
	delwin(decor);
}


std::string dialogWindow(chstring name, size_t w, bool hidden, int CB, int FC) {
	size_t width = (name.length() > w ? name.length() + 2 : w + 2);
	window dial(3, width, getmaxy(stdscr)/2-2, getmaxx(stdscr)/2-width/2, CB, FC); 
	char buffer[256];
	mvwaddchstr(dial.decor, 0, getmaxx(dial.decor)/2 - name.length()/2, name.c_str());
	wrefresh(dial.decor);
	if(!hidden)
		echo();
	curs_set(TRUE);
	mvwgetstr(dial.win, 0, 0, buffer);
	if(!hidden)
		noecho();
	curs_set(FALSE);
	wclear(dial.decor);
	wrefresh(dial.decor);
	return std::string(buffer);
}


void messageWindow(chstring name, chstring message, int CB, int FC) {
	size_t width = (name.length() > message.length() ? name.length() + 2 : message.length() + 2);
	window mes(3, width, getmaxy(stdscr)/2-2, getmaxx(stdscr)/2-width/2, CB, FC);
	mvwaddchstr(mes.decor, 0, getmaxx(mes.decor)/2 - name.length()/2, name.c_str());
	mvwaddchstr(mes.decor, 1, getmaxx(mes.decor)/2 - message.length()/2, message.c_str());
	wrefresh(mes.decor);
	while(wgetch(mes.win)!='\n');
	wclear(mes.decor);
	wrefresh(mes.decor);
}



menuElement::menuElement(chstring Data, bool Select, int Allign) : 
		data(Data), 
		select(Select), 
		allign(Allign)  {
	
}

menuList::menuList() : n(0) {
}
void menuList::add(chstring line, bool sel, int allign) {
	list.push_back(menuElement(line, sel, allign));
	if((!sel)&&(n==list.size()-1))
		++n;
}
void menuList::add(std::string line, bool sel, int allign, chtype attr) {
	add(chstr(line, attr), sel, allign);
}
void menuList::draw(WINDOW* win, size_t y, size_t x, size_t w) {
	int r = 0;
	for(size_t i=0; i<list.size(); i++) {
		if(i==n) 
			r = A_REVERSE;
		if(list[i].allign==0)
			mvwaddchstr(win, y+i, x, (list[i].data|r).c_str());
		else if(list[i].allign==1)
			mvwaddchstr(win, y+i, x+w/2-list[i].data.length()/2, (list[i].data|r).c_str());
		else
			mvwaddchstr(win, y+i, x+w-list[i].data.length(), (list[i].data|r).c_str());				
		if(i==n) 
			r = 0;
	}
	wrefresh(win);
}
void menuList::clear() {
	list.clear();
	n = 0;
}
void menuList::operator =(int x) {
	n = x;
}
void menuList::operator ++() {
	if(n>=list.size())
		return;
	int o = n;
	++n;
	if(n>=list.size())
		n=0;
	while(!list[n].select) {
		++n;
		++o;
		if(n>=list.size())
			n=0;
		if(o==n)
			return;
	}
	if(n>=list.size())
		n=0;
}
void menuList::operator --() {
	if(n>=list.size())
		return;
	int o = n;
	--n;
	if(n<=-1)
		n=list.size()-1;
	while(!list[n].select) {
		--n;
		if(n<=-1)
			n=list.size()-1;
		if(o==n)
			return;
	}
	if(n<=-1)
		n=list.size()-1;
}
menuList::operator int() {
	return n;
}
chstring& menuList::operator [](size_t i) {
	return list[i].data;
}

logger::logger() : 
		_status(false),  
		_win(nullptr), 
		_page(0) {
}
	
void logger::setWin(WINDOW* W) {
	_win = W;
}
void logger::start() {
	boost::unique_lock<boost::mutex> lock(_mut);
	_status = true;
	update();
}
void logger::stop() {
	_status = false;
}
void logger::update(chstring str) {
	boost::unique_lock<boost::mutex> lock(_mut);
	_log.push_back(str);
	if(getmaxy(_win)<_log.size())
		++_page;
	if(_status)
		update();
}
void logger::update() {
	for(size_t i=_page; (i-_page<getmaxy(_win))&&(i<_log.size()); ++i)  {
		mvwaddchstr(_win, i-_page, 0, _log[i].c_str());
		wmove(_win, i-_page, _log[i].length());
		wclrtoeol(_win);
	}
	wrefresh(_win);
}
bool logger::status() {
	return _status;
}
void logger::operator ++() {
	boost::unique_lock<boost::mutex> lock(_mut);
	++_page;
	if(_page+getmaxy(_win)>=_log.size()) {
		_page = _log.size()-getmaxy(_win);
		if(_page < 0)
			_page=0;
	}
	else
		update();
}
void logger::operator --() {
	boost::unique_lock<boost::mutex> lock(_mut);
	--_page;
	if(_page<0)
		_page=0;
	else
		update();
}

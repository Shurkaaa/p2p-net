#pragma once
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <memory>
#include <string>
using namespace boost::asio;


struct clientData {
	std::shared_ptr<ip::tcp::socket> sock;
	std::shared_ptr<ip::tcp::endpoint> ep;
	std::shared_ptr<boost::mutex> mut;
	clientData(io_service& ios);
	clientData(io_service& ios, std::string address, uint16_t port);
	void reconnect(boost::system::error_code& ec);
	void read(boost::asio::streambuf& buf, size_t n, boost::system::error_code& ec);
	void write(boost::asio::streambuf& buf, boost::system::error_code& ec);
	~clientData();
};


typedef void (*acceptFunction)(const boost::system::error_code&);
typedef	void (*handleFunction)(std::vector<std::shared_ptr<clientData>>&, std::shared_ptr<boost::mutex>, std::shared_ptr<clientData>);
	

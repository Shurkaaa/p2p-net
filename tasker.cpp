#include "tasker.h"

task::task() :
		_task_id((size_t)this+rand()),
		_handle(nullptr){
}

task::task(const task& other) :
		_task_id(other._task_id),
		_data(other._data),
		_handle(other._handle){
}

task::task(task&& other) :
		_task_id(other._task_id),
		_data(other._data),
		_handle(other._handle){
}

task::task(taskHandle function) :
		_task_id((size_t)this+rand()),
		_handle(function) {			
}

task& task::operator =(task&& other) {
	_task_id = other._task_id;
	_data = other._data;
	_handle = other._handle;
	return *this;
}

void task::push_back(std::string data) {
	_data.push_back(data);
}

void task::operator ()() {
	_handle(_data);
}

void tasker::run() {
	while(_status) {
		_mut.lock();
		if(_tasks.size() == 0) {
			_mut.unlock();
			_working = false;
			continue;
		}
		_working = true;
		task curTask(std::move(_tasks[0]));
		_tasks.erase(_tasks.begin());
		_mut.unlock();
		curTask();
	}
}

tasker::tasker() :
		_status(false),
		_working(false),
		_mut(),
		_tasks(),
		_thread() {
}

void tasker::push_back(task&& data) {
	boost::unique_lock<boost::mutex> lk(_mut);
	_tasks.push_back(data);
}

void tasker::start() {
	_status = true;
	_working = true;
	_thread.reset(new boost::thread(&tasker::run, this));
}

void tasker::stop() {
	_status = false;
	_thread->join();
}

bool tasker::status() {
	return _status;
}

bool tasker::working() {
	return _working;
}

#include "clientData.h"


clientData::clientData(io_service& ios) : 
		sock(std::make_shared<ip::tcp::socket>(ios)), 
		ep(std::make_shared<ip::tcp::endpoint>()),
		mut(std::make_shared<boost::mutex>()){
}

clientData::clientData(io_service& ios, std::string address, uint16_t port) : 
		sock(std::make_shared<ip::tcp::socket>(ios)), 
		ep(std::make_shared<ip::tcp::endpoint>(ip::address::from_string(address), port)),
		mut(std::make_shared<boost::mutex>()){
}

void clientData::reconnect(boost::system::error_code& ec) {
	boost::unique_lock<boost::mutex> lock(*mut);
	sock->close();
	sock->connect(*ep, ec);	
}

void clientData::read(boost::asio::streambuf& buf, size_t n, boost::system::error_code& ec) {
	boost::unique_lock<boost::mutex> lock(*mut);
	boost::asio::read(*sock, buf, transfer_exactly(n), ec);
}
void clientData::write(boost::asio::streambuf& buf, boost::system::error_code& ec) {
	boost::unique_lock<boost::mutex> lock(*mut);
	boost::asio::write(*sock, buf, ec);
}

clientData::~clientData() {
	sock->close();
}

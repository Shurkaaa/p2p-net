#include "p2p_web.h"



void closeWebConnection(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current) {
	boost::unique_lock<boost::mutex> lk(*mut);
	current->sock->close();
	for(size_t i = 0; i < clients.size(); ++i) 
		if(clients[i] == current) {
			clients.erase(clients.begin() + i);
			break;
		}
}


string getErrorMessage(size_t err) {
	string errmes;
	switch(err) {
		case 400:
		errmes="400 Bad Request";
		break;
		case 401:
		errmes="401 Unauthorized";
		break;
		case 402:
		errmes="402 Payment Required";
		break;
		case 403:
		errmes="403 Forbidden";
		break;
		case 404:
		errmes="404 Not Found";
		break;
		case 405:
		errmes="405 Method Not Allowed";
		break;
		case 406:
		errmes="406 Not Acceptable";
		break;
		case 407:
		errmes="407 Proxy Authentication Required";
		break;
		case 408:
		errmes="408 Request Timeout";
		break;
		case 409:
		errmes="409 Conflict";
		break;
		case 410:
		errmes="410 Gone";
		break;
		case 411:
		errmes="411 Length Required";
		break;
		case 412:
		errmes="412 Precondition Failed";
		break;
		case 413:
		errmes="413 Request Entity Too Large";
		break;
		case 414:
		errmes="414 Request-URI Too Large";
		break;
		case 415:
		errmes="415 Unsupported Media Type";
		break;
		case 416:
		errmes="416 Requested Range Not Satisfiable";
		break;
		case 417:
		errmes="417 Expectation Failed";
		break;
		case 422:
		errmes="422 Unprocessable Entity";
		break;
		case 423:
		errmes="423 Locked";
		break;
		case 424:
		errmes="424 Failed Dependency";
		break;
		case 425:
		errmes="425 Unordered Collection";
		break;
		case 426:
		errmes="426 Upgrade Required";
		break;
		case 428:
		errmes="428 Precondition Required";
		break;
		case 429:
		errmes="429 Too Many Requests";
		break;
		case 431:
		errmes="431 Request Header Fields Too Large";
		break;
		case 434:
		errmes="434 Requested host unavailable";
		break;
		case 449:
		errmes="449 Retry With";
		break;
		case 451:
		errmes="451 Unavailable For Legal Reasons";
		break;
		case 500:
		errmes="500 Internal Server Error";
		break;
		case 501:
		errmes="501 Not Implemented";
		break;
		case 502:
		errmes="502 Bad Gateway";
		break;
		case 503:
		errmes="503 Service Unavailable";
		break;
		case 504:
		errmes="504 Gateway Timeout";
		break;
		case 505:
		errmes="505 HTTP Version Not Supported";
		break;
		case 506:
		errmes="506 Variant Also Negotiates";
		break;
		case 507:
		errmes="507 Insufficient Storage";
		break;
		case 508:
		errmes="508 Loop Detected";
		break;
		case 509:
		errmes="509 Bandwidth Limit Exceeded";
		break;
		case 510:
		errmes="510 Not Extended";
		break;
		case 511:
		errmes="511 Network Authentication Required";
		break;
	}
	return errmes;
}

string getMIME(string ext) {
	if(ext == ".html") 
		return "text/html";
	else if((ext == ".jpg")||(ext == ".jpeg"))
		return "image/jpeg";
	else if(ext == ".css")
		return "text/css";
	else if(ext == ".js")
		return "text/javascript";
	return "text/plain";
}

string servername = "p2p server";
string getErrorResponse(size_t err) {
	string errmes = getErrorMessage(err);
	return "<html>\r\n\
<head><title>"+errmes+"</title></head>\r\n\
<body bgcolor=\"white\">\r\n\
<center><h1>"+errmes+"</h1></center>\r\n\
<hr><center>"+servername+"</center>\r\n\
</body>\r\n\
</html>\r\n";
}
void handleByWebServer(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current) {
	boost::system::error_code ec;
	netBuffer inbuf;
	netBuffer outbuf;
	bool fl = true;
	date UTCd = day_clock::universal_day();
	ptime UTCt = second_clock::universal_time();
	string datetime =  	string(UTCd.day_of_week().as_short_string()) + ", " + 
						to_string(UTCd.day()) + " " + 
						string(UTCd.month().as_short_string()) + " " + 
						to_string(UTCd.year()) + " " +
						to_iso_extended_string(UTCt).substr(11) + " GMT";
	//cout << datetime;
	
	do {
		size_t n = read_until(*(current->sock), inbuf.get(), "\n", ec);
		if(ec) {
			//cout << "Can't read request: " << ec.message() << endl;
			closeWebConnection(clients, mut, current);
			return;
		}
		string line(n, ' ');
		inbuf.read(&line[0], n);
		stringstream ss(line); //TODO: operator >> and << in netBuffer overload
		string sline;
		ss >> sline;
		if(fl) {
			string URN;
			string ver;
			ss >> URN >> ver;
			URN = "www" + URN;
			//URN = URN.substr(1);
			string resp;
			string type;
			string content;
			string connection;
			if(ver != "HTTP/1.1") {
				resp = getErrorMessage(400);
				content = getErrorResponse(400);
				type = getMIME(".html");;
				connection = "close";
			}
			if(sline=="GET") {
				if(URN=="www/")
					URN="www/index.html";
				if(exists(path(URN))) {
					type = getMIME(extension(path(URN)));

					std::ifstream file(URN.c_str(), ios_base::binary);
					if(!file.is_open()) {
						resp = getErrorMessage(500);
						content = getErrorResponse(500);
						type = getMIME(".html");;
						connection = "close";
					}
					else {
						while(1) {
							//string data;
							char c = file.get();
							//getline(file, data);
							if(!file)
								break;
							content.push_back(c);
							//content += data + "\r\n";
						}
						file.close();
						resp = "200 OK";
						//type = getMIME(".html");
						connection = "close";
					}
				}
				else {
					
					resp = getErrorMessage(404);
					content = getErrorResponse(404);
					type = getMIME(".html");;
					connection = "close";
				}
			}
			else if(sline=="HEAD") {
				if(URN=="")
					URN="index.html";
				if(exists(path(URN))) {
					resp = "204 No Content";
					type = getMIME(".html");;
					connection = "close";
				}
				else {
					resp = getErrorMessage(404);
					content = getErrorResponse(404);
					type = getMIME(".html");;
					connection = "close";
				}
			}
			else {
				resp = getErrorMessage(501);
				content = getErrorResponse(501);
				type = getMIME(".html");;
				connection = "close";
			}
			outbuf.writeString("HTTP:/1.1 " + resp + "\r\n");
			outbuf.writeString("Date: " + datetime + "\r\n");
			outbuf.writeString("Server: " + servername + "\r\n");
			if(resp!="204 No Content\r\n") {
				outbuf.writeString("Content-Type: " + type + "\r\n");
				outbuf.writeString("Content-Length: " + to_string(content.length()) + "\r\n");
			}
			outbuf.writeString("Connection: " + connection + "\r\n");
			outbuf.writeString("\r\n");
			if(resp!="204 No Content\r\n") 
				outbuf.writeString(content);
			
			write(*(current->sock), outbuf.get(), ec);
			if(ec) {
				//cout << "Can't write answer: "  << ec.message() << endl;
				closeWebConnection(clients, mut, current);
				return;
			}
			closeWebConnection(clients, mut, current);
			return;
		}
		if(line=="\r\n")
			break;
	} while(1);
}


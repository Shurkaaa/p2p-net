#include "asyncServer.h"
#include <iostream>

void asyncServer::initAccept() {
	_mutex->lock();
	_clients.push_back(std::make_shared<clientData>(_ios));
	std::shared_ptr<clientData> temp = _clients[_clients.size()-1];
//	std::cout << "(" << (void*)temp << ")" << std::endl;
	_mutex->unlock();
	_acceptor.async_accept(*(temp->sock), *(temp->ep), [this, temp](const  boost::system::error_code& ec) {
//		std::cout << "{" << (void*)temp << "}" << std::endl;
		_accept(ec);
		if(!ec) 
			_threadPool.addWork(_clientHandle, _clients, _mutex, temp);
		else {
			boost::unique_lock<boost::mutex> lk(*_mutex);
			for(auto i = _clients.begin(); i != _clients.end(); ++i)
				if(*i == temp) {
					_clients.erase(i);
					break;
				}
		}
		initAccept();
	});
}


void asyncServer::run() {
	_threadPool.start(4);  //TODO: Magic number
	_acceptor.listen();
	initAccept();
	_ios.run();
	_status = false;
}

//asyncServer::asyncServer(void (*accept)(const boost::system::error_code&), void (*clientHandle)(std::vector<std::shared_ptr<clientData>>&, std::shared_ptr<boost::mutex>, std::shared_ptr<clientData>)) :
asyncServer::asyncServer(acceptFunction accept, handleFunction clientHandle) :
		_status(false), 
		_acceptor(_ios),
		_mutex(std::make_shared<boost::mutex>()),
		_accept(accept),
		_clientHandle(clientHandle){
}

void asyncServer::start(uint16_t port) {
	_acceptor = ip::tcp::acceptor(_ios, ip::tcp::endpoint(ip::address_v4(), port));
	_thread.reset(new boost::thread(&asyncServer::run, this));
	_status = true;
}

void asyncServer::stop() {
	if(_status) {
		_ios.stop();
		_thread->join();
		_threadPool.join();
		_threadPool.stop();
	}
}

bool asyncServer::status() {
	return _status;
}

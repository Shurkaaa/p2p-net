#pragma once
#include "clientData.h"
#include "threadPool.h"
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <atomic>
#include <memory>
#include <string>
using namespace boost::asio;

class asyncServer {
    std::atomic<bool> _status;
    std::unique_ptr<boost::thread> _thread;
    io_service _ios;
	ip::tcp::acceptor _acceptor;
	std::vector<std::shared_ptr<clientData>> _clients;
	std::shared_ptr<boost::mutex> _mutex;
	threadPool _threadPool;
	acceptFunction _accept;
	handleFunction _clientHandle;
	void initAccept();
	void run();
public:
	asyncServer(acceptFunction accept, handleFunction clientHandle);
	void start(uint16_t port);
    void stop();
	bool status();
};


#include "netBuffer.h"

netBuffer::netBuffer() : stream(&buf) {}
void netBuffer::writeByte(const uint8_t a)  {
	stream.put(a);
}
void netBuffer::writeShort(const uint16_t a) {
	uint16_t b = htons(a);
	stream.write((char*)&b, 2);
}
void netBuffer::writeLong(const uint32_t a){
	uint32_t b = htonl(a);
	stream.write((char*)&b, 4);
}
void netBuffer::writeString(const std::string& str) {
	stream << str;
}
void netBuffer::writeUstring(const ustring& str) {
	stream << (const char*)str.c_str();
}
uint8_t netBuffer::readByte() {
	return stream.get();
}
uint16_t netBuffer::readShort() {
	uint16_t b;
	stream.read((char*)&b, 2);
	return ntohs(b);
}
uint32_t netBuffer::readLong() {
	uint32_t b;
	stream.read((char*)&b, 4);
	return ntohl(b);
}
std::string netBuffer::readString() {
	std::string a;
	stream >> a;
	return a;
}
ustring netBuffer::readUstring() {
	std::string a;
	stream >> a;
	return ustring((unsigned char*)a.c_str());
}
void netBuffer::read(void* data, size_t len) {
	stream.read((char*)data, len);
}
void netBuffer::write(const void* data, size_t len) {
	stream.write((const char*)data, len);
}
void netBuffer::clear() {
	stream.clear();
}
size_t netBuffer::size() {
	return buf.size();
}
boost::asio::streambuf& netBuffer::get() {
	return buf;
}

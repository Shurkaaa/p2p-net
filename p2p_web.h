#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include "asyncServer.h"
#include "netBuffer.h"
using namespace std;
using namespace boost::asio;
using namespace boost::filesystem;
using namespace boost::gregorian;
using namespace boost::posix_time;


string getErrorMessage(size_t err);
string getMIME(string ext);
void handleByWebServer(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current);
